package com.test.auth;

/**
 * @author Karpov Oleg
 */
public interface AuthApi {
    AuthInteractor getAuthInteractor();
}
