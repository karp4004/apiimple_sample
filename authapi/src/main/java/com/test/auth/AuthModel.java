package com.test.auth;

/**
 * @author Karpov Oleg
 */
public class AuthModel {
    private String text;

    public AuthModel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
