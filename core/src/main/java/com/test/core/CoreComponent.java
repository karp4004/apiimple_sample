package com.test.core;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component
public interface CoreComponent extends CoreApi {
}
