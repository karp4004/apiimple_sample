package com.test.core;

import androidx.annotation.NonNull;

/**
 * Базовый класс-обертка для получения инстанса API некоторой фичи
 *
 * <p>Базовый холдер не имеет реализации освобождения инстанса API фичи</p>
 *
 * @param <T> тип хранимого инстанса фичи
 * @author Dmitriy Tarasov on 09/11/2018
 */
public abstract class BaseFeatureHolder<T> implements FeatureHolder<T> {

    private static final String TAG = "DI:BaseFeatureHolder";

    protected final FeatureContainer mFeatureContainer;

    /**
     * Экземпляр Api фичи
     */
    protected T mFeature;

    /**
     * Базовый холдер фичи
     *  @param featureContainer {@link FeatureContainer} используется для поиска зависимостей фичи
     *
     */
    public BaseFeatureHolder(@NonNull FeatureContainer featureContainer) {
        mFeatureContainer = featureContainer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public T getFeature() {
        if (mFeature == null) {
            mFeature = buildFeature();
        }
        return mFeature;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void releaseFeature() {
        // метод намеренно оставле пустым т.к. базовый холдер не реализует освобождение инстанса API фичи
    }

    /**
     * Получает инстанс зависимости, необходимой данной фиче
     *
     * @param key {@link Class} типа зависимости
     * @param <D> тип зависимости
     * @return инстанс зависимости
     */
    @NonNull
    protected <D> D getDependency(Class<D> key) {
        return mFeatureContainer.getDependency(key);
    }

    /**
     * Создает {@link #mFeature} подставляя используя необходимые зависимости
     */
    @NonNull
    protected abstract T buildFeature();

}
