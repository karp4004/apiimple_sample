package com.test.core;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Karpov Oleg
 */
public class CoreActivity extends AppCompatActivity {
    @NonNull
    protected <T> T getFeature(@NonNull Class<T> featureApiClass) {
        return getFeatureInternal(featureApiClass);
    }

    @NonNull
    private <T> T getFeatureInternal(@NonNull Class<T> featureApiClass) {
        return FeatureExtractionUtils.getFeature(this, featureApiClass);
    }
}
