package com.test.core;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Scope всех фичевых апи, предоставляемых в рамках системы работы с di, описанной
 * <a href='https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=778113375'>здесь</a>.
 *
 * @author Evgeny Chumak
 **/
@Scope
@Documented
@Retention(RUNTIME)
public @interface PerFeature {
}
