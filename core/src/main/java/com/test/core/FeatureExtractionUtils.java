package com.test.core;

import android.content.Context;

import androidx.annotation.NonNull;

public final class FeatureExtractionUtils {

    private FeatureExtractionUtils() {
        throw new AssertionError("Utility classes should not be instantiated");
    }

    @NonNull
    public static <T> T getFeature(@NonNull Context context, @NonNull Class<T> key) {
        return getContainer(context).getFeature(key);
    }

    @NonNull
    @SuppressWarnings("unchecked,ConstantConditions")
    public static <T> T getInternalFeature(@NonNull Context context, Class key, Class<T> returnType) {
        Object feature = getFeature(context, key);
        if (returnType.isInstance(feature)) {
            return returnType.cast(feature);
        } else {
            throw new IllegalStateException("Feature with key " + key + " should be instance of " + returnType);
        }
    }


    @NonNull
    public static <T> T getDependency(@NonNull Context context, @NonNull Class<T> key) {
        return getContainer(context).getDependency(key);
    }


    @NonNull
    @SuppressWarnings("unchecked,ConstantConditions")
    public static <T> T getInternalDependency(@NonNull Context context, Class key, Class<T> returnType) {
        Object feature = getDependency(context, key);
        if (returnType.isInstance(feature)) {
            return returnType.cast(feature);
        } else {
            throw new IllegalStateException("Feature with key " + key + " should be instance of " + returnType);
        }
    }

    public static void releaseFeature(@NonNull Context context, @NonNull Class<? extends ReleasableApi> key) {
        getContainer(context).releaseFeature(key);
    }

    @NonNull
    public static FeatureContainer getContainer(@NonNull Context context) {
        return ((FeatureContainer) context.getApplicationContext());
    }

}
