package com.test.core;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

/**
 * Контейнер экземпляра фичи
 *
 * </p>
 *
 * @author Шкурко Александр
 */
public interface FeatureHolder<T> {

    /**
     * Возвращает инстанс фичи
     * <p>
     * Если на момент вызова инстанс фичи не создан, то он создаётся вместе со всеми инстансами зависимых фичей
     * </p>
     *
     * @return Проинициализированный экземпляр фичи
     */
    @NonNull
    @MainThread
    T getFeature();

    /**
     * Зануляет хранимый инстанс API фичи и вызывает зануление зависимых фичей в соответствующих {@link FeatureHolder}
     */
    @MainThread
    void releaseFeature();
}
