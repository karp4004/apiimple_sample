package com.test.core;

import androidx.annotation.NonNull;

public interface FeatureContainer {

    @NonNull
    <T> T getFeature(Class<T> key);


    @NonNull
    <T> T getDependency(Class<T> key);

    void releaseFeature(Class<? extends ReleasableApi> key);
}
