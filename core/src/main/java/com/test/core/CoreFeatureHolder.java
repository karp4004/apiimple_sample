package com.test.core;

import androidx.annotation.NonNull;

public class CoreFeatureHolder extends BaseFeatureHolder<CoreApi> {

    public CoreFeatureHolder(FeatureContainer featureContainer) {
        super(featureContainer);
    }

    @NonNull
    @Override
    protected CoreApi buildFeature() {
        return DaggerCoreComponent.builder()
                .build();
    }

    @Override
    public void releaseFeature() {
    }
}
