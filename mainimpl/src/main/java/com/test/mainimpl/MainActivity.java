package com.test.mainimpl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.test.auth.AuthModel;
import com.test.core.CoreActivity;
import com.test.mainapi.MainApi;
import com.test.mainapi.MainModel;
import com.test.supportapi.SupportApi;
import com.test.supportapi.SupportModel;

public class MainActivity extends CoreActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(v ->
                getFeature(SupportApi.class).getRouter().openSupport(
                        new MainModel("MainModel argument"),
                        new AuthModel("AuthModel argument"),
                        this
                )
        );
    }
}
