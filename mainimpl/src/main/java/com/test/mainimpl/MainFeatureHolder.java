package com.test.mainimpl;

import androidx.annotation.NonNull;

import com.test.core.BaseFeatureHolder;
import com.test.core.CoreApi;
import com.test.core.FeatureContainer;
import com.test.mainapi.MainApi;

public class MainFeatureHolder extends BaseFeatureHolder<MainApi> {

    public MainFeatureHolder(FeatureContainer featureContainer) {
        super(featureContainer);
    }

    @NonNull
    @Override
    protected MainApi buildFeature() {
        MainDependencies mainDependencies =
                DaggerMainComponent_MainDependenciesComponent.builder()
                        .coreApi(getDependency(CoreApi.class))
                        .build();
        return DaggerMainComponent.builder()
                .mainDependencies(mainDependencies)
                .build();
    }

    @Override
    public void releaseFeature() {
    }
}
