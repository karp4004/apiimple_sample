package com.test.mainimpl;

import android.app.Activity;
import android.content.Intent;

import com.test.auth.AuthModel;
import com.test.mainapi.MainModel;
import com.test.mainapi.MainRouter;

/**
 * @author Karpov Oleg
 */
public class MainRouterImpl implements MainRouter {
    @Override
    public void openMain(AuthModel authModel,
                         Activity activity) {
        activity.startActivity(new Intent(activity, MainActivity.class));
    }
}
