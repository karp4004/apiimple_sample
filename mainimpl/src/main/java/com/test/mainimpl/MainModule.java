package com.test.mainimpl;

import com.test.core.PerFeature;
import com.test.mainapi.MainInteractor;
import com.test.mainapi.MainRouter;

import dagger.Module;
import dagger.Provides;

@Module
public interface MainModule {

    @PerFeature
    @Provides
    static MainInteractor provideMainInteractor() {
        return new MainInteractor();
    }

    @PerFeature
    @Provides
    static MainRouter provideMainRouter() {
        return new MainRouterImpl();
    }
}

