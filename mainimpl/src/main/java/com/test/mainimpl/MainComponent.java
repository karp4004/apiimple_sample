package com.test.mainimpl;

import com.test.core.CoreApi;
import com.test.core.PerFeature;
import com.test.mainapi.MainApi;

import dagger.Component;

@Component(
        dependencies = {MainDependencies.class},
        modules = {
                MainModule.class
        }
)
@PerFeature
public interface MainComponent extends MainApi {

    @Component(dependencies = {
            CoreApi.class
    })
    @PerFeature
    interface MainDependenciesComponent extends MainDependencies {
    }
}
