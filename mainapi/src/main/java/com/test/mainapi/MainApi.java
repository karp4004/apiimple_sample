package com.test.mainapi;

/**
 * @author Karpov Oleg
 */
public interface MainApi {
    MainInteractor getMainInteractor();

    MainRouter getMainRouter();
}
