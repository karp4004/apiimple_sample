package com.test.mainapi;

/**
 * @author Karpov Oleg
 */
public class MainModel {
    private String text;

    public MainModel(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
