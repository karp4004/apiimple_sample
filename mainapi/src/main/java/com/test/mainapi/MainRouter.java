package com.test.mainapi;

import android.app.Activity;

import com.test.auth.AuthModel;

/**
 * @author Karpov Oleg
 */
public interface MainRouter {
    void openMain(AuthModel mainModel,
                  Activity activity);
}
