package com.test.apiimpl;

import android.app.Application;

import androidx.annotation.NonNull;

import com.test.core.FeatureContainer;
import com.test.core.FeatureHolder;
import com.test.core.ReleasableApi;

import java.util.Map;

/**
 * @author Karpov Oleg
 */
public class TestApp extends Application implements FeatureContainer {

    private Map<Class<?>, FeatureHolder> mFeatures;

    @Override
    public void onCreate() {
        super.onCreate();
        mFeatures = buildProdFeatureHolders();
    }

    protected Map<Class<?>, FeatureHolder> buildProdFeatureHolders() {
        return DaggerProdFeatureHoldersComponent
                .builder()
                .application(this)
                .build()
                .getFeatureHolders();
    }

    @NonNull
    @Override
    public <T> T getFeature(Class<T> key) {
        return getFeatureHolder(key).getFeature();
    }

    @NonNull
    @Override
    public <T> T getDependency(Class<T> key) {
        return getFeature(key);
    }

    @Override
    public void releaseFeature(Class<? extends ReleasableApi> key) {
    }

    @NonNull
    private  <T> FeatureHolder<T> getFeatureHolder(Class<T> key) {
        return mFeatures.get(key);
    }
}
