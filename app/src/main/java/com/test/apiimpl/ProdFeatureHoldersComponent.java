package com.test.apiimpl;

import androidx.core.view.KeyEventDispatcher;

import com.test.core.FeatureHolder;

import java.util.Map;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Компонент, предоставляющий фичахолдеры, используемые в рамках пользовательской сессии
 *
 * @author Evgeny Chumak
 **/
@Component(modules = {
        ProdOtherFeatureHoldersModule.class
})
@Singleton
public interface ProdFeatureHoldersComponent {

    Map<Class<?>, FeatureHolder> getFeatureHolders();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(TestApp application);

        ProdFeatureHoldersComponent build();
    }
}
