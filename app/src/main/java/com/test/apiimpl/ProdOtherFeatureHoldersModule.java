package com.test.apiimpl;

import android.content.Context;

import androidx.annotation.NonNull;

import com.test.auth.AuthApi;
import com.test.authimpl.AuthFeatureHolder;
import com.test.core.CoreApi;
import com.test.core.CoreFeatureHolder;
import com.test.core.FeatureContainer;
import com.test.core.FeatureHolder;
import com.test.mainapi.MainApi;
import com.test.mainimpl.MainFeatureHolder;
import com.test.supportapi.SupportApi;
import com.test.supportimpl.SupportFeatureHolder;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module
public interface ProdOtherFeatureHoldersModule {

    @Singleton
    @Provides
    @IntoMap
    @ClassKey(CoreApi.class)
    static FeatureHolder provideCoreFeatureHolder(FeatureContainer featureContainer) {
        return new CoreFeatureHolder(featureContainer);
    }

    @Singleton
    @Provides
    @IntoMap
    @ClassKey(AuthApi.class)
    static FeatureHolder provideAuthFeatureHolder(FeatureContainer featureContainer) {
        return new AuthFeatureHolder(featureContainer);
    }

    @Singleton
    @Provides
    @IntoMap
    @ClassKey(MainApi.class)
    static FeatureHolder provideMainFeatureHolder(FeatureContainer featureContainer) {
        return new MainFeatureHolder(featureContainer);
    }

    @Singleton
    @Provides
    @IntoMap
    @ClassKey(SupportApi.class)
    static FeatureHolder provideSupportFeatureHolder(FeatureContainer featureContainer) {
        return new SupportFeatureHolder(featureContainer);
    }

    @Singleton
    @Binds
    Context bindApplicationContext(TestApp application);

    @Singleton
    @Binds
    FeatureContainer bindFeatureContainer(TestApp application);
}
