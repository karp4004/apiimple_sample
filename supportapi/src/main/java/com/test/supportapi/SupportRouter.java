package com.test.supportapi;

import android.app.Activity;

import com.test.auth.AuthModel;
import com.test.mainapi.MainModel;

/**
 * @author Karpov Oleg
 */
public interface SupportRouter {
    void openSupport(MainModel mainModel,
                     AuthModel authModel,
                     Activity activity);
}
