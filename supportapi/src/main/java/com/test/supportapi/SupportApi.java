package com.test.supportapi;

/**
 * @author Karpov Oleg
 */
public interface SupportApi {
    SupportInteractor getSupportInteractor();
    SupportRouter getRouter();
}
