package com.test.authimpl;

import androidx.annotation.NonNull;

import com.test.auth.AuthApi;
import com.test.core.BaseFeatureHolder;
import com.test.core.CoreApi;
import com.test.core.FeatureContainer;
import com.test.core.FeatureHolder;

public class AuthFeatureHolder extends BaseFeatureHolder<AuthApi> {

    public AuthFeatureHolder(FeatureContainer featureContainer) {
        super(featureContainer);
    }

    @NonNull
    @Override
    protected AuthApi buildFeature() {
        AuthDependencies authDependencies =
                DaggerAuthComponent_AuthDependenciesComponent.builder()
                        .coreApi(getDependency(CoreApi.class))
                        .build();
        return DaggerAuthComponent.builder()
                .authDependencies(authDependencies)
                .build();
    }

    @Override
    public void releaseFeature() {
    }
}
