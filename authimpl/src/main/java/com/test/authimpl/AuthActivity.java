package com.test.authimpl;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.test.auth.AuthModel;
import com.test.core.CoreActivity;
import com.test.mainapi.MainApi;
import com.test.mainapi.MainModel;

public class AuthActivity extends CoreActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        findViewById(R.id.button).setOnClickListener(v ->
                getFeature(MainApi.class).getMainRouter().openMain(new AuthModel("AuthModel argument"), this)
        );
    }
}
