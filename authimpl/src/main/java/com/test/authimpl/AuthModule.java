package com.test.authimpl;

import com.test.auth.AuthInteractor;
import com.test.core.PerFeature;

import dagger.Module;
import dagger.Provides;

@Module
public interface AuthModule {

    @PerFeature
    @Provides
    static AuthInteractor provideAuthInteractor() {
        return new AuthInteractor();
    }
}

