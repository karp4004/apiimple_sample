package com.test.supportimpl;

import androidx.annotation.NonNull;

import com.test.core.BaseFeatureHolder;
import com.test.core.CoreApi;
import com.test.core.FeatureContainer;
import com.test.supportapi.SupportApi;

public class SupportFeatureHolder extends BaseFeatureHolder<SupportApi> {

    public SupportFeatureHolder(FeatureContainer featureContainer) {
        super(featureContainer);
    }

    @NonNull
    @Override
    protected SupportApi buildFeature() {
        SupportDependencies supportDependencies =
                DaggerSupportComponent_SupportDependenciesComponent.builder()
                        .coreApi(getDependency(CoreApi.class))
                        .build();
        return DaggerSupportComponent.builder()
                .supportDependencies(supportDependencies)
                .build();
    }

    @Override
    public void releaseFeature() {
    }
}
