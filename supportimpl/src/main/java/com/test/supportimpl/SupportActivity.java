package com.test.supportimpl;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.test.auth.AuthApi;
import com.test.auth.AuthModel;
import com.test.core.CoreActivity;
import com.test.mainapi.MainApi;
import com.test.mainapi.MainModel;
import com.test.supportapi.SupportApi;

public class SupportActivity extends CoreActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        TextView textView = findViewById(R.id.text_view);
        textView.setText(
                getFeature(AuthApi.class).getAuthInteractor().getAuthModel().getText() + "\n" +
                getFeature(MainApi.class).getMainInteractor().getMainModel().getText()
        );
    }
}
