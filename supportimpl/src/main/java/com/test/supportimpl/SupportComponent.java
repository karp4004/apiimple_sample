package com.test.supportimpl;

import com.test.core.CoreApi;
import com.test.core.PerFeature;
import com.test.supportapi.SupportApi;

import dagger.Component;

@Component(
        dependencies = {SupportDependencies.class},
        modules = {
                SupportModule.class
        }
)
@PerFeature
public interface SupportComponent extends SupportApi {

    @Component(dependencies = {
            CoreApi.class
    })
    @PerFeature
    interface SupportDependenciesComponent extends SupportDependencies {
    }
}
