package com.test.supportimpl;

import com.test.core.PerFeature;
import com.test.mainapi.MainInteractor;
import com.test.supportapi.SupportInteractor;
import com.test.supportapi.SupportRouter;

import dagger.Module;
import dagger.Provides;

@Module
public interface SupportModule {

    @PerFeature
    @Provides
    static SupportInteractor provideMainInteractor() {
        return new SupportInteractor();
    }

    @PerFeature
    @Provides
    static SupportRouter provideSupportRouter() {
        return new SupportRouterImpl();
    }
}

