package com.test.supportimpl;

import android.app.Activity;
import android.content.Intent;

import com.test.auth.AuthModel;
import com.test.mainapi.MainModel;
import com.test.supportapi.SupportModel;
import com.test.supportapi.SupportRouter;

/**
 * @author Karpov Oleg
 */
public class SupportRouterImpl implements SupportRouter {

    @Override
    public void openSupport(MainModel mainModel, AuthModel authModel, Activity activity) {
        activity.startActivity(new Intent(activity, SupportActivity.class));
    }
}
